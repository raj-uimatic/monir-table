/**
 *
 *  App v0.1.1
 *
 * */

$.fn.exfiltrate = function (selector) {
    return this.filter(selector).add(this.find(selector));
};


var tableId = 'question_table';
var delimiter = {
    question: ';',
    answer: ','
};

function init(xmlSnippet) {
    var htmlObject = $.parseHTML(xmlSnippet);
    // modifyHtml modify existing htmlObject object into final html
    modifyHtml(htmlObject);
    $("#html").html('');
    // for now I am appending the whole snippet as it is just replacing <simpleMatchSet> with a table crated by xml data
    $("#html").append(htmlObject);
}

// function to create whole html
function modifyHtml(htmlObject) {
    // Group both simpleMatchSet together
    var tableXml = $(htmlObject).exfiltrate('simpleMatchSet');
    // wrap simpleMatchSet in a single tag <qTable>
    $(tableXml).wrapAll("<qTable></qTable>");
    // Get table html
    var tableHtml = createHtml(htmlObject);
    // qtable object
    var qtable = $(htmlObject).find('qtable');
    qtable.replaceWith(tableHtml);
    // no need to return htmlObject this line can be removed if not required
    return htmlObject;
}

// this function returns html of table
function createHtml(htmlObject) {
    var rows = $(htmlObject).find('simpleMatchSet:first');
    var column = $(htmlObject).find('simpleMatchSet:last');

    var table = $('<table>', {id: tableId}).append('<thead><tr><th>Statement</th>').append('<tbody>');
    var thead = table.find('thead tr');
    $(rows).find('simpleAssociableChoice').each(function (rowkey, row) {
        // Create row
        var tr = $('<tr>', {'data-identifier': $(row).attr('identifier')});
        // Append row to tbody
        table.find('tbody').append(tr);
        // now create first column which will contain expression or data
        tr.append('<td>').html($(row).html());
        $(column).find('simpleAssociableChoice').each(function (key, value) {
            // create thead answer columns
            if (rowkey === 1) {
                thead.append('<th>' + $(value).attr('identifier') + '</th>');
            }
            // now create column which will contain checkbox
            var td = $('<td>');
            // now create checkbox
            var checkbox = $('<input>', {
                'type': 'checkbox',
                'value': $(value).attr('identifier'),
                'data-identifier': $(value).attr('identifier')
            });
            // append column with checkbox to main row(tr)
            tr.append(td.append(checkbox));
        });
    });
    return table;
}

//This Function generate response string and paste response string in a textbox
function getResponseString() {
    var allTr = $('#' + tableId + ' tbody tr');
    var responseString = '';
    $.each(allTr, function (k, tr) {
        var row_identifier = $(tr).data('identifier');
        $.each($(tr).find('input:checked'), function (k, v) {
            responseString += (responseString === '') ? row_identifier + delimiter.answer + $(v).val() : delimiter.question + row_identifier + delimiter.answer + $(v).val();
        });
    });
    $('#response_string').val(responseString);
}


// function to fill data in table
function fillResponseString() {
    var dataString = $.trim($('#fill_response_string').val());
    dataString = dataString.split(delimiter.question);
    $.each(dataString, function (key, qaSet) {
        var qa = qaSet.split(delimiter.answer);
        var row = $('#' + tableId).find('tr[data-identifier="' + qa[0] + '"]');
        var checkbox = row.find('input[data-identifier="' + qa[1] + '"]');
        checkbox.prop('checked', true);
    });
}